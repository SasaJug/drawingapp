package com.sasaj.graphics.paintselector.com.sasaj.graphics.paintselector.interfaces;

/**
 * Created by User on 8/27/2016.
 */
public interface PaintSelector {

    public int getColor();
    public void setColor(int color);
}
