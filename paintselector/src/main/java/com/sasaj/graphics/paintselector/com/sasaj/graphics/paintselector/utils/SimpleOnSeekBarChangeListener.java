package com.sasaj.graphics.paintselector.com.sasaj.graphics.paintselector.utils;

import android.widget.SeekBar;

/**
 * Created by User on 8/27/2016.
 */
public class SimpleOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener{

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
