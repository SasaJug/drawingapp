## README##

### Drawing Application  ###

* Android app for on-screen drawing
* Created for purpose of development of "paint selector", widget for choosing Android Paint Object's size, blur, alpha and color. The widget will be developed as standalone library. 

### To implement in PaintSelector: ###

* On configuration change persistence
* Responsiveness to different screen sizes and orientations

### To implement in DrawingApp ###

* More efficient drawing surface
* Photoshop-like layers
* Editing existing drawings capability

* Screenshots - present look:

![Screenshot_2017-04-23-19-05-03.png](https://bitbucket.org/repo/9a9649/images/2483177284-Screenshot_2017-04-23-19-05-03.png)
![Screenshot_2017-04-23-19-06-01.png](https://bitbucket.org/repo/9a9649/images/3447355535-Screenshot_2017-04-23-19-06-01.png)